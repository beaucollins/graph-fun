/**
 * @flow
 */
import type { Grid } from './';

export type Point = {| x: number, y: number |};
export type Value = 0 | 1 | 9;

function row({ y }: Point): number {
    return y;
}

function column({ x }: Point): number {
    return x;
}

export function cellValue(grid: Grid<Value>, point: Point): Value {
    const first = grid[row(point)];
    if (first === undefined) {
        return 0;
    }
    const value = first[column(point)];
    if (value === undefined) {
        return 0;
    }
    return value;
}

export function cellNeighbors(grid: Grid<Value>, { x, y }: Point) : Array<Point> {
    return [
        { x, y: y + 1 },
        { x, y: y - 1 },
        { y, x: x + 1 },
        { y, x: x - 1 },
    ].filter(point => cellValue(grid, point) !== 0);
}

export function isDestination(grid: Grid<Value>, point: Point) : boolean {
    return cellValue(grid, point) === 9;
}
