/**
 * @flow
 */

export type Node<T> = {|
    value: T,
    parent: ?Node<T>
|}

export type Grid<T> = Array<Array<T>>;
type Size = [number, number];

export function pathTo<V, T>(
    grid: Grid<V>,
    origin: T,
    identify: (T) => string,
    isDestination: (Grid<V>, T) => boolean,
    neigborsFor: (Grid<V>, T) => Array<T>,
    path: Array<T> = [],
): ?Node<T> {
    const queue: Array<Node<T>> = [{ value: origin, parent: null }];
    const visited: Set<string> = new Set([identify(origin)]);
    while (queue.length > 0) {
        const current = queue.shift();
        if (isDestination(grid, current.value)) {
            return current;
        }
        for(const edge of neigborsFor(grid, current.value)) {
            const id = identify(edge);
            if (visited.has(id)) {
                continue;
            }
            visited.add(id);
            queue.push({ value: edge, parent: current });
        }
    }
    return null;
}

export function getPath<T>(node: Node<T>): Array<T> {
    if (node.parent) {
        return [node.value, ...getPath(node.parent)];
    } else {
        return [node.value];
    }
}

export function sizeOf<T>(grid: Grid<T>): Size {
    return [
        grid.reduce((width, row) => Math.max(width, row.length), 0),
        grid.length,
    ];
}