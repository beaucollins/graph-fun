/**
 * @flow
 */
import type { Grid } from './';
import { sizeOf } from './';

type Coordinates = { x: number, y: number };

export function render<T>(grid: Grid<T>, draw: (T, Coordinates) => string) {
    const [width, height] = sizeOf(grid);
    return grid.reduce(
        (graph, row, y) =>
            graph + '│' + fill(width, (x) => ' ' + draw(row[x], { x, y })) + ' │\n',
        header(width),
    ) + footer(width);
}

function header(size) {
    return '┌' + fill(size, () => '──') + '─┐' + '\n';
}

function footer(size) {
    return '└' + fill(size, () => '──') + '─┘';
}

function fill(size, draw: (number) => string) {
    let graph = '';
    for(let i=0; i<size; i++) {
        graph += draw(i);
    }
    return graph;
}
