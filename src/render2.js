/**
 * @flow
 */
import type { Grid } from './';
type Coordinate = {| x: number, y: number |};

export default function render<T, D>(
    grid: Grid<T>,
    graph: D,
    draw: (T, Coordinate, D) => D
) {
    return grid.reduce<D>(
        (graph, row, y) => 
            row.reduce<D>(
                (graph, value, x) =>  draw(value, {x, y}, graph),
                graph,
            )
        ,
        graph
    );
}