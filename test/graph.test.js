/**
 * @flow
 */
import type { Grid, Node } from '../src';
import type { Point, Value } from '../src/point';

import { pathTo, getPath, sizeOf } from '../src';
import { isDestination, cellNeighbors, cellValue } from '../src/point';
import { render } from '../src/render';

describe('graph', () =>{

    const grid: Grid<Value> = [
        [1,1,0],
        [1,0,0],
        [1,1,9],
    ];

    const grid2: Grid<Value> = [
        [1,1,0,1,1],
        [1,1,0,1,1],
        [1,1,1,1,1],
        [1,1,0,1,9],
    ];

    const grid3: Grid<Value> = [
        [0, 0, 1],
        [0, 9, 1],
        [1, 1, 1],
    ];

    const grid4: Grid<Value> = [
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 0, 1, 0, 0, 0, 0, 1],
        [1, 0, 0, 1, 1, 1, 1, 1],
        [1, 0, 1, 0, 0, 1, 0, 0],
        [1, 0, 1, 0, 0, 1, 0, 1],
        [1, 0, 1, 1, 9, 1, 0, 1],
        [1, 0, 1, 0, 0, 1, 0, 1],
        [1, 1, 1, 0, 1, 1, 1, 1],
    ];

    type Fixture = [Grid<Value>, number];

    const fixtures = [
        [grid, 5],
        [grid2, 8],
        [grid3, 0],
        [grid4, 14],
    ];

    fixtures.forEach(([grid, expected], index) => {
        it(`finds paths ${index}`, () => {
            const path = pathTo(
                grid,
                { x: 0, y: 0 },
                ({x, y}) => `${x},${y}`,
                isDestination,
                cellNeighbors,
            );
            const steps = nodeToPath(path);
            console.log(render(grid, (value, coordinates) => {
                const index = steps.reduce(
                    (index, {x, y}, i) => {
                        if (index > -1) {
                            return index;
                        }
                        if (coordinates.x === x && coordinates.y === y) {
                            return i;
                        }
                        return index;
                    },
                    -1
                );
                switch (index) {
                    case -1: {
                        switch(value) {
                            case 0: {
                                return ' ';
                            }
                            case 1: {
                                return '◦';
                            }
                            case 9: {
                                return '✗';
                            }
                        }
                    };
                    case 0: return '⦿';
                    case steps.length-1: return '✘';
                    default: return '•';
                }
            }));
            expect(steps.length).toBe(expected);
        });

    })
});

function nodeToPath<T>(node: ?Node<T>): Array<T> {
    if (node == null) {
        return [];
    }

    let current = node;
    const list: Array<T> = [];
    while(current) {
        list.unshift(current.value);
        current = current.parent;
    }
    return list;
}

function cellCharacter(value: Value, coordinate, grid): string {
    if (value === 0) {
        return ' ';
    }
    const neighbors = cellNeighbors(grid, coordinate);
    const { N, S, E, W } = cardinals(coordinate, neighbors);

    if (N && S && E && W) {
        return '┼';
    }
    if (N && S && E && !W) {
        return '├';
    }
    if (N && S && !E && W) {
        return '┤';
    }
    if (!N && S && E && W) {
        return '┬';
    }
    if (N && !S && E && W) {
        return '┴';
    }
    if (N && S && !E && !W) {
        return '│';
    }
    if (!N && !S && E && W) {
        return '─';
    }
    if (!N && S && E && !W) {
        return '┌';
    }
    if (!N && S && !E && W) {
        return '┐';
    }
    if (N && !S && E && !W) {
        return '└';
    }
    if (N && !S && !E && W) {
        return '┘';
    }
    if (N && !S && !E && !W) {
        return '╵';
    }
    if (!N && S && !E && !W) {
        return '╷';
    }
    if (!N && !S && E && !W) {
        return '╶';
    }
    if (!N && !S && !E && W) {
        return '╴';
    }
    return ' ';
}

function cardinals(point, neighbors) {
    if (neighbors.length === 4) {
        return { N: true, S: true, E: true, W: true };
    }
    if (neighbors.length === 0) {
        return { N: false, S: false, E: false, W: false };
    }
    return neighbors.reduce(
        (directions, neighbor) => {
            if (neighbor.x > point.x) {
                return { ...directions, E: true };
            } else if (neighbor.x < point.x) {
                return { ...directions, W: true };
            }
            if (neighbor.y > point.y) {
                return { ...directions, S: true };
            } else if (neighbor.y < point.y) {
                return { ...directions, N: true };
            }
            return directions;
        },
        { N: false, S: false, E: false, W: false },
    );
}
